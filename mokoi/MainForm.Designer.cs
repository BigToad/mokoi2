﻿namespace Mokoi
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.Button_Select_Folder = new System.Windows.Forms.Button();
            this.TextBox_Folder = new System.Windows.Forms.TextBox();
            this.Button_Convert = new System.Windows.Forms.Button();
            this.TextBox_FileType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBox_FileEncoding = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.TextBox_Keyword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Button_Select_Folder
            // 
            this.Button_Select_Folder.Location = new System.Drawing.Point(397, 21);
            this.Button_Select_Folder.Name = "Button_Select_Folder";
            this.Button_Select_Folder.Size = new System.Drawing.Size(75, 23);
            this.Button_Select_Folder.TabIndex = 0;
            this.Button_Select_Folder.Text = "Select";
            this.Button_Select_Folder.UseVisualStyleBackColor = true;
            this.Button_Select_Folder.Click += new System.EventHandler(this.SelectFolder_Click);
            // 
            // TextBox_Folder
            // 
            this.TextBox_Folder.Location = new System.Drawing.Point(76, 23);
            this.TextBox_Folder.Name = "TextBox_Folder";
            this.TextBox_Folder.Size = new System.Drawing.Size(315, 20);
            this.TextBox_Folder.TabIndex = 1;
            // 
            // Button_Convert
            // 
            this.Button_Convert.Location = new System.Drawing.Point(194, 128);
            this.Button_Convert.Name = "Button_Convert";
            this.Button_Convert.Size = new System.Drawing.Size(75, 23);
            this.Button_Convert.TabIndex = 2;
            this.Button_Convert.Text = "Convert";
            this.Button_Convert.UseVisualStyleBackColor = true;
            this.Button_Convert.Click += new System.EventHandler(this.Convert_Click);
            // 
            // TextBox_FileType
            // 
            this.TextBox_FileType.Location = new System.Drawing.Point(76, 75);
            this.TextBox_FileType.Name = "TextBox_FileType";
            this.TextBox_FileType.Size = new System.Drawing.Size(315, 20);
            this.TextBox_FileType.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Folder:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "File type:";
            // 
            // TextBox_FileEncoding
            // 
            this.TextBox_FileEncoding.Location = new System.Drawing.Point(76, 49);
            this.TextBox_FileEncoding.Name = "TextBox_FileEncoding";
            this.TextBox_FileEncoding.Size = new System.Drawing.Size(315, 20);
            this.TextBox_FileEncoding.TabIndex = 7;
            this.TextBox_FileEncoding.MouseHover += new System.EventHandler(this.TextBox_FileEncoding_MouseHover);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Encoding:";
            // 
            // TextBox_Keyword
            // 
            this.TextBox_Keyword.Location = new System.Drawing.Point(76, 101);
            this.TextBox_Keyword.Name = "TextBox_Keyword";
            this.TextBox_Keyword.Size = new System.Drawing.Size(315, 20);
            this.TextBox_Keyword.TabIndex = 9;
            this.TextBox_Keyword.MouseHover += new System.EventHandler(this.TextBox_Keyword_MouseHover);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Keyword:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 163);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextBox_Keyword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TextBox_FileEncoding);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBox_FileType);
            this.Controls.Add(this.Button_Convert);
            this.Controls.Add(this.TextBox_Folder);
            this.Controls.Add(this.Button_Select_Folder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Mokoi";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Button_Select_Folder;
        private System.Windows.Forms.TextBox TextBox_Folder;
        private System.Windows.Forms.Button Button_Convert;
        private System.Windows.Forms.TextBox TextBox_FileType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBox_FileEncoding;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.TextBox TextBox_Keyword;
		private System.Windows.Forms.Label label4;
	}
}

