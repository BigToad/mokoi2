﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace Mokoi
{
    public partial class MainForm : Form
    {

        public MainForm()
        {
            InitializeComponent();
        }

        /*
         *  Allow the user to select a folder from dialog
         */
        private void SelectFolder_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                TextBox_Folder.Text = dialog.FileName;
            }
        }

        /*
         * Start converting the files in the given folder
         */
        private void Convert_Click(object sender, EventArgs e)
        {
            try
            {
				int convertCount = 0;
				String[] keywords = TextBox_Keyword.Text.Split(',');
				foreach (var file in Directory.GetFiles(TextBox_Folder.Text))
                {
                    String encoding = "";
					//Try looking for the file encoding if the user gave none
                    if (string.IsNullOrEmpty(TextBox_FileEncoding.Text)){
                        Stream fs = File.OpenRead(file);
                        encoding = DetectFileEncoding(fs);
                        fs.Close();
                    }
                    else
                    {
                        encoding = TextBox_FileEncoding.Text;
                    }
					//Get the decoded hex
                    string decoded = GetDecodedString(file, encoding);
					//Create a file if the keywords are found
					if (Search(keywords, decoded)){
						CreateFile(decoded, file, encoding);
						convertCount++;
					}
                }
				MessageBox.Show("Converted " + convertCount + " files.", "Done converting",
				MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
            catch( Exception ex)
            {
            MessageBox.Show(ex.Message, "Error",
            MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


           
        }
		
		/*
		 * Search for the given keywords in the decoded hex and return true if all of them are found
		 */
		public bool Search(String[] keywords, String decodedHex)
		{
			foreach (String key in keywords)
			{
				if (decodedHex.IndexOf(key, StringComparison.OrdinalIgnoreCase) == -1)
				{
					return false;
				}
			}
			
			return true;
		}



        /*
         * Decode the hex values of a file and return it as a string
         */
        private string GetDecodedString(String file, String encoding)
        {
            try
            {
                FileStream fs = new FileStream(file, FileMode.Open);              
                int hexIn;
                List<byte> data = new List<byte>();
                for (int i = 0; (hexIn = fs.ReadByte()) != -1; i++)
                {
                    data.Add((byte)hexIn);
                }
                string result = Encoding.GetEncoding(encoding).GetString(data.ToArray());
                fs.Close();
                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        /*
         * Checks if the hexdata of the given file contains a reference to the requested file type and creates a new file
         */
        private void CreateFile(String hexData, String file, String encoding)
        {

            if (hexData.Contains(TextBox_FileType.Text))
            {
                String newFileName = file + "." + TextBox_FileType.Text;
                if (!File.Exists(newFileName))
                {
                    string s = hexData.Substring(hexData.LastIndexOf(TextBox_FileType.Text));
                    byte[] raw = Encoding.GetEncoding(encoding).GetBytes(s);
                    using (Stream f = File.OpenWrite(newFileName))
                    {
                        f.Write(raw, 0, raw.Length);
                    }
                }
            }
        }

        /*
         * Try to determine the encoding of a file
         */
        private string DetectFileEncoding(Stream fileStream)
        {
            var Utf8EncodingVerifier = Encoding.GetEncoding("utf-8", new EncoderExceptionFallback(), new DecoderExceptionFallback());
            using (var reader = new StreamReader(fileStream, Utf8EncodingVerifier,
                   detectEncodingFromByteOrderMarks: true, leaveOpen: true, bufferSize: 1024))
            {
                string detectedEncoding;
                try
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                    }
                    detectedEncoding = reader.CurrentEncoding.BodyName;
                }
                catch (Exception)
                {
                    // Failed to decode the file using the BOM/UT8. 
                    // Assume it's local ANSI
                    detectedEncoding = "ISO-8859-1";
                }
                // Rewind the stream
                fileStream.Seek(0, SeekOrigin.Begin);
                return detectedEncoding;
            }
        }

        private void TextBox_FileEncoding_MouseHover(object sender, EventArgs e)
        {
            toolTip1.Show("If left empty the program will try to determine the encoding", TextBox_FileEncoding);
        }

		private void TextBox_Keyword_MouseHover(object sender, EventArgs e)
		{
			toolTip1.Show("Only convert files that contain the given keyword in the hex. Add multiple by separating each keyword with ',' ", TextBox_FileEncoding);
		}
	}
}
